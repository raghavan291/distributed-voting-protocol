#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <pthread.h>

#define DEBUG
#define BACKLOG 10
#define NOOFTHREADS 2000 // NEED TO CHECK HOW MANY REQUESTS ARE PRESENT
#define MAX_KEYS 100
#define TRUE 1
#define FALSE 0

typedef struct key_value_t
{
        int key;
        void *value;
        int version_num;
        int lock;
        int lock_set;
}key_value_table;

typedef struct 
{
	int hostname[100];
	int port;
	int socket;
} thread_param_t;

void *ThreadManager(void *arg)
{
	thread_param_t *clstr = (thread_param_t *)arg;	
	int res;
	char server_buf[1024];
	if((res=recv(clstr->socket, server_buf, 1024,MSG_WAITALL)) < 0)
        {	
			perror("An error occurred in the recv\n");
 	}
	server_buf[res]='\0';
	#ifdef DEBUG
		printf("Request of the following client being served\n");
        	printf("Socket descriptor - %d\n",clstr->socket);
                printf("Client IP address - %s\n",clstr->hostname);
                printf("Client port - %d\n",clstr->port);
        #endif
	printf("Server received msg : %s",server_buf);
	
	

	fflush(stdout);
	
	free(clstr);
}


void usage()
{
	printf("Improper comandline arguments");
}

/*

key_value_table *kv_table;
int no_of_keys;
int probability;        //probability is a number bw 90 - 100
int lier;

//check probability and decide whether to vote or not
void sendVote(void *hostname, int port, int key)
{
	int first, random_no;
        srand(time(NULL));
	//generate a random number from 1 - 10
	random_no = (rand() % (10));
	if(!(random_no == (100 - probability)))
	{
		//add code to send 1 to vote
	}
	else
	{
		//add code to send 0 to vote
	}
}

void sendVersionNumber(char *hostname, int port, int key, char *request)
{
        int i, version_num;
        for(i=0; i < no_of_keys; i++)
        {
                if(kv_table[i].key == key)
                {
		        version_num = kv_table[i].version_num;	
			break;
		}
	}
        //If the server does not have an entry for the requested key then it will always vote and informs about the Version No as 0     
	if(lier == 1)
	{
		if(request == "PUT")
		{
			//modify version number
			version_num = 2;
		}
		//and it can also lie about key value in both GET and PUT according to message board
	}
	printf("\nsending version No %d to %s %d", version_num, hostname, port);	
        //add code to send version number to the client
}

void sendKeyValue(char *hostname, int port, int key)
{
	int i;
	char *value = (char *) malloc(sizeof(char) * 200);
	for(i=0; i < no_of_keys; i++)
	{
		if(kv_table[i].key == key)
		{
			value =  (char *) kv_table[i].value;
			if(lier == 1)
			{
				//modify value
				value = "XYZ";
			}
			printf("\nsending key %d value %s to %s %d\n", key, value, hostname, port);
			//add code to send key, value pair to the client
			if(lier == 1)
				printf("\nlied to %s %d about %d\n", hostname, port, key);
			return;
		}
	}
}

void lockKey(int key)
{
	int i;
        for(i=0; i < no_of_keys; i++)
        {
                if(kv_table[i].key == key)
                {
			pthread_mutex_lock(&kv_table[i].lock);	
			kv_table[i].lock_set = TRUE;
			printf("\nlock on %d", key);
			return;
		}
	}
	
	//if no key found, create a new key and put a lock on it
	kv_table[no_of_keys].key = key;
	pthread_mutex_lock(&kv_table[no_of_keys].lock);
	kv_table[no_of_keys].lock_set = TRUE;
	no_of_keys++;
	printf("\nlock on %d", key);
}

void updateKeyValue(int key, void * value, int new_version)
{
	int i;
        for(i=0; i < no_of_keys; i++)
        {
                if(kv_table[i].key == key)
                {		
			printf("\nwriting key %d value %s with updated version no %d\n", key, (char *) value, new_version);
                        kv_table[i].value = (void *) value;
                        kv_table[i].version_num = new_version;
		}
	}
}

void unlockKey(int key)
{
 	int i;
        for(i=0; i < no_of_keys; i++)
        {
                if(kv_table[i].key == key)
                {
			pthread_mutex_unlock(&kv_table[i].lock);
			kv_table[i].lock_set = FALSE;
		}
	}
}

int main(int argc, char *argv[])
{
	char *hostname = (char *) malloc(sizeof(char) * 20);
	hostname = "127.0.0.1";
	char *value = (char *) malloc(sizeof(char) * 200);
	int key, version_num, port;
	int i, num;

	//Format for requests -
	//0) request for getting vote - PUT:vote:key 
	//1) request for getting version num - PUT:version_num:key
	//2) request for putting value and value - PUT:value:version_num:key
	//3) request to unlock - PUT:unlock:key
	//4) request for getting vote - GET:vote:key
	//5) request for getting version num - GET:version_num:key
	//6) request for getting value - GET:value:key
	

	char requests[7][50];
	strcpy(requests[0], "PUT:vote:100");
	strcpy(requests[1], "PUT:version_num:100");
	strcpy(requests[2], "PUT:Navya:1:100");
	strcpy(requests[3], "PUT:unlock:100");
        strcpy(requests[4], "GET:vote:100");
        strcpy(requests[5], "GET:version_num:100");
        strcpy(requests[6], "GET:value:100");

	char *request;
	char *command, *next, *token, *temp;

	kv_table = (key_value_table *) malloc(sizeof(key_value_table) * MAX_KEYS);	
	if(argc != 4)
	{
		printf("\nUsage: ./server <port> <prob of replying back> <lier>\n");
		exit(1);
	}

	port = atoi(argv[1]);
	probability = (atoi(argv[2]) * 100);
	lier = atoi(argv[3]);

	//listen for data from the client 
	for(num = 0; num < 7; num++)
	{
		request = requests[num];
		temp = strtok_r(request, ":", &next);
		//if GET request 
		if(strcmp(temp, "GET") == 0)
		{
                        temp = strtok_r(NULL, ":", &next);
			command = temp;
			temp =  strtok_r(NULL, ":", &next);
			if(strcmp(command, "vote") == 0)
			{
				key = atoi(temp);
	                        printf("\ncontacted by %s %d for GET %d", hostname, port, key);
				sendVote(hostname, port, key);
			}
			else if(strcmp(command, "version_num") == 0)
			{
				key = atoi(temp);
				sendVersionNumber(hostname, port, key, "GET");
			}
			else if(strcmp(command, "value") == 0)
			{
				key = atoi(temp);
				sendKeyValue(hostname, port, key);
			}
		}
		else if(strcmp(temp, "PUT") == 0)
		{
			temp = strtok_r(NULL, ":", &next);
			command = temp;
                        temp =  strtok_r(NULL, ":", &next);
                        if(strcmp(command, "vote") == 0)
			{
				key = atoi(temp);
	                        printf("\ncontacted by %s %d for PUT %d",hostname, port, key);	
				lockKey(key);
				//check if lock is set and send vote
				for(i=0; i < no_of_keys; i++)
        			{
                			if((kv_table[i].key == key) && (kv_table[i].lock_set == TRUE))
                			{
						sendVote(hostname, port, key);
					}
				}
			}
			else if(strcmp(command, "version_num") == 0)
                        {
                                key = atoi(temp);
                                sendVersionNumber(hostname, port, key, "PUT");
                        }
                        else if(strcmp(command, "unlock") == 0)
			{
                                key = atoi(temp);
				unlockKey(key);
			}
			else
                        {
				value = command;
				version_num = atoi(temp);
				temp = strtok_r(NULL, ":", &next);
                                key = atoi(temp);
                                updateKeyValue(key, value, version_num);
                        }

		}
	}
}


*/

int main(int argc, char *argv[])
{

	int server_soc = 0,server_acc_soc=0,server_port,set=1;
	struct sockaddr_in server_addr,client_addr;
	char server_buf[1024];
	FILE *fp;
	char hname_port[100];
	int clen,k=0;
	pthread_t thr[NOOFTHREADS];  //THREADS COUNT MAINTAIN	

	if(argc == 2)
	{
		server_port = atoi(argv[1]);

		if((gethostname(hname_port,100))<0 )
                {
		#ifdef DEBUG
                        perror("Get hostname failed ");
                #endif
		}
                else
                {

		#ifdef DEBUG
                        printf("Hostname is %s and port is %s\n",hname_port,argv[1]);
		#endif

                }

		fp = fopen("server_loc.txt","a");
                if(fp == NULL)
                {
                #ifdef DEBUG
                        perror("Cannot open server_loc file");
                #endif
                }               
                else
                {
                        strcat(hname_port," ");
                        strcat(hname_port,argv[1]);
                        strcat(hname_port,"\n");
			fputs(hname_port,fp);
                        fclose(fp);
                }     

	
		if(((server_soc = socket(AF_INET,SOCK_STREAM,0)) < 0))
		{
			perror("Server socket creation failed ");
			return errno;	
		}

		server_addr.sin_family = AF_INET;
		server_addr.sin_addr.s_addr =  INADDR_ANY;
		server_addr.sin_port = htons(server_port); 
		memset(server_addr.sin_zero, '\0', sizeof (server_addr.sin_zero));

		if((bind(server_soc, (struct sockaddr *)&server_addr, sizeof(server_addr))) < 0)
		{
			close(server_soc);
			perror("Bind failed ");
			return errno;
		}

		if(setsockopt(server_soc,SOL_SOCKET, SO_REUSEADDR,&set,sizeof(set)) < 0)
		{
			close(server_soc);
                        perror("Setsockopt failed ");
                        return errno;
		}
	
		if((listen(server_soc, BACKLOG)) < 0)
		{
			close(server_soc);
			perror("Listen failed ");
			return errno;
		}


		clen = sizeof (struct sockaddr_in);

		while(1)
		{
		if(((server_acc_soc = accept(server_soc,(struct sockaddr *)&client_addr,&clen)) < 0))
		{
			perror("Accept failed ");			
		}
		else
		{
		#ifdef DEBUG
			printf("New client connected from port no %d and IP address %s\n", ntohs(client_addr.sin_port), inet_ntoa(client_addr.sin_addr));
		#endif

		thread_param_t *arg = (thread_param_t *)malloc(sizeof(thread_param_t)); 
		
		arg->socket = server_acc_soc;
		arg->port = ntohs(client_addr.sin_port);
		strcpy(arg->hostname,inet_ntoa(client_addr.sin_addr)); 
	

		#ifdef DEBUG
			printf("Socket descriptor - %d\n",arg->socket);
			printf("Client IP address - %s\n",arg->hostname);
			printf("Client port - %d\n",arg->port);
		#endif
		fflush(stdout);

		if((pthread_create(&thr[k++], NULL, ThreadManager, (void *)arg))< 0)
		{
			perror("Thread creation failed ");
		}
		}

		}

		//sleep(10);

		if((shutdown(server_soc,2)) < 0)
		{
			perror("Error in socket termination ");
		}
		close(server_soc);

	}
	else
	{
		usage();
	}
	return 0;
	
}

