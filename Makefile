CC        =   gcc
CPPFLAGS  =   -g -w
O_FILES   =   server client
SERVER    =   server.c
CLIENT    =   client.c
LIB       =   -pthread 

all: server client

server:
	$(CC) $(CPPFLAGS) $(LIB) $(SERVER) -o server

client:
	$(CC) $(CPPFLAGS) $(LIB) $(CLIENT) -o client

clean:
	rm -rf $(O_FILES) 
