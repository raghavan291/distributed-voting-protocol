#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <pthread.h>

#define N 1

#define DEBUG
#define NOOFTHREADS 2000 // NEED TO CHECK HOW MANY REQUESTS ARE PRESENT

typedef struct
{
        int hostname[100];
        int port;
        int socket;
} thread_param_t;

void *ThreadManager(void *arg)
{
        
	thread_param_t *srstr = (thread_param_t *)arg;
        int res;
        char client_buf[1024];
        
        if((res = send(srstr->socket,client_buf,strlen(client_buf),0)) < 0)
        {
                perror("Error in send ");
        }

        #ifdef DEBUG
        printf("Client sent data and bytes is %s and %d\n",client_buf,res);
        #endif

	#ifdef DEBUG
                printf("Server for whom message is sent\n");
                printf("Socket descriptor - %d\n",srstr->socket);
                printf("Server IP address - %s\n",srstr->hostname);
                printf("Server port - %d\n",srstr->port);
        #endif

	fflush(stdout);

	free(srstr);	
}



void usage()
{
	printf("Improper commandline arguments");
}

int main(int argc, char *argv[])
{
	int client_soc[N],n;
	pthread_t thr[NOOFTHREADS];
	struct sockaddr_in remote_server_addr[N];
	char remote_server_ip[N][100], remote_server_port[N][6];
	struct hostent *hp;	
	
	int i=0,j=0,k=0,total_servers=2;
	
	/*int client_soc = 0,remote_server_port,n;
        struct sockaddr_in remote_server_addr;
	char remote_server_ip[16];*/
	
	char client_buf[1024]= "This is a message to server";
	FILE *cfp;

        if(argc == 3)
        {
                
		cfp = fopen("server_loc.txt","r");
		if(cfp == NULL)
		{
		#ifdef DEBUG
			total_servers=0;
			perror("Cannot server location file ");
		#endif		
		}
		else
		{

			while(!feof(cfp))
			{
				fscanf(cfp,"%s",remote_server_ip[i]);
				fscanf(cfp,"%s",remote_server_port[i]);
				i++;
			}
			total_servers = i-2; //Last line is null from file read
			fclose(cfp);
		}

		#ifdef DEBUG
			for(i=0;i<=total_servers;i++)
			{
				printf("Server hostname is %s ",remote_server_ip[i]);
				printf("Server port is %s\n",remote_server_port[i]);
			}
		#endif				

		//strcpy(remote_server_ip,argv[1]);
		//remote_server_port = atoi(argv[2]);

                if(((client_soc[N-1] = socket(AF_INET,SOCK_STREAM,0)) < 0))
                {
                        perror("Client socket creation failed ");
                }

                remote_server_addr[N-1].sin_family = AF_INET;

                remote_server_addr[N-1].sin_addr.s_addr = inet_addr(remote_server_ip[N-1]); 
                remote_server_addr[N-1].sin_port = htons(atoi(remote_server_port[N-1])); 
                memset(remote_server_addr[N-1].sin_zero, '\0', sizeof (remote_server_addr[N-1].sin_zero));
	
		
		if((connect(client_soc[N-1], (struct sockaddr *)&remote_server_addr[N-1], sizeof(remote_server_addr[N-1]))) < 0)
                {
                        close(client_soc[N-1]);
                        perror("Connect failed ");
                }
		else
                {
                #ifdef DEBUG
                        printf("Client connected to server port no %d and server IP address %s\n", ntohs(remote_server_addr[N-1].sin_port), inet_ntoa(remote_server_addr[N-1].sin_addr));
                #endif

                thread_param_t *arg = (thread_param_t *)malloc(sizeof(thread_param_t));

                arg->socket = client_soc[N-1];
                arg->port = ntohs(remote_server_addr[N-1].sin_port);
                strcpy(arg->hostname,inet_ntoa(remote_server_addr[N-1].sin_addr));


                #ifdef DEBUG
                        printf("Socket descriptor - %d\n",arg->socket);
                        printf("Server IP address - %s\n",arg->hostname);
                        printf("Server port - %d\n",arg->port);
                #endif
                fflush(stdout);

                if((pthread_create(&thr[k++], NULL, ThreadManager, (void *)arg))< 0)
                {
                        perror("Thread creation failed ");
                }
                }

		if((shutdown(client_soc[N-1],2)) < 0)
                {
                        perror("Error in socket termination ");
                }
                close(client_soc[N-1]);

        }
        else
        {
                usage();
        }
        return 0;
}
